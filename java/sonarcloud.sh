#!/bin/sh

HTTP_STATUS=$(curl -s -X GET -u "${SONAR_TOKEN}:" "https://sonarcloud.io/api/project_branches/list?project=${CI_PROJECT_ROOT_NAMESPACE}_${CI_PROJECT_NAME}" -o /dev/null -w '%{http_code}\n')

if [ $HTTP_STATUS != 200 ]; then
  	curl -s -X POST -u "${SONAR_TOKEN}:" "https://sonarcloud.io/api/projects/create" -d "name=${CI_PROJECT_NAME}&project=${CI_PROJECT_ROOT_NAMESPACE}_${CI_PROJECT_NAME}&organization=${CI_PROJECT_ROOT_NAMESPACE}&visibility=${CI_PROJECT_VISIBILITY}"
  	if [ ${CI_DEFAULT_BRANCH} == "main" ]; then
  		curl -s -X POST -u "${SONAR_TOKEN}:" "https://sonarcloud.io/api/project_branches/rename?project=${CI_PROJECT_ROOT_NAMESPACE}_${CI_PROJECT_NAME}&name=${CI_DEFAULT_BRANCH}"	
  	fi
fi
