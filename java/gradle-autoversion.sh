#!/bin/sh
echo "Agrega como directorio seguro ${CI_PROJECT_DIR}"
git config --global --add safe.directory ${CI_PROJECT_DIR}
echo "Agrega datos de usuario que disparó el job: $GITLAB_USER_NAME - $GITLAB_USER_EMAIL"
git config --global user.email "$GITLAB_USER_EMAIL"
git config --global user.name "$GITLAB_USER_NAME"
echo "Obteniendo titulo del ultimo commit"
SUBJECT=`git log -1 --pretty=%s`
echo "El ultimo commit fue $SUBJECT"
if [ `echo $SUBJECT | grep -c "Merge "` -ge 1 ]
    then
        echo "Obteniendo version actual del ultimo tag creado. En su defecto de la version indicada en gradle.properties"
        currentVersion=$(git describe --tag --abbrev=0 || cat gradle.properties | grep 'version' | cut -d'=' -f2)
        echo "Versión actual -> $currentVersion"

        major=$(echo $currentVersion | cut -d. -f1)
        minor=$(echo $currentVersion | cut -d. -f2)
        patch=$(echo $currentVersion | cut -d. -f3)
        if
            [ `echo $SUBJECT | grep -c "feature-"` -ge 1 ] ||
            [ `echo $SUBJECT | grep -c "minor-"` -ge 1 ]
        then
            echo 'minor change'
            minor=$((minor+1))
            patch=0
        else
            if
                [ `echo $SUBJECT | grep -c "patch-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "fix-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "hotfix-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "doc-"` -ge 1 ]
            then
                echo 'patch change'
                patch=$((patch+1))
            else
                if
                    [ `echo $SUBJECT | grep -c "major-"` -ge 1 ]
                then
                    echo 'major change'
                    major=$((major+1))
                    minor=0
                    patch=0
                else
                    echo "No se ha podido determinar la siguiente version. Por favor incluye alguna de las siguientes palabras en el nombre del branch:"
                    echo "patch, fix, hotfix o doc para incrementar el ultimo digito."
                    echo "feature o minor para incrementar el segundo digito."
                    echo "major para incrementar el primer digito."
                    exit 1
            fi
        fi
    fi

    releasedVersion="$major.$minor.$patch"
    echo "Versión a generar -> $releasedVersion"
    #Actualiza version del componente
    sed -ie s/^version=.*/version=$releasedVersion/ gradle.properties

    echo "Se sube foto inicial de Reglas para Archunit"
    archunitDir=archunit
    if [ -d "$archunitDir" ];
    then
        git add gradle.properties $archunitDir/
    else
        git add gradle.properties
    fi
    
    git commit -m "Tag version $releasedVersion"
    # Configura URL para usar token y así no enviar usuario y contrasenia por HTTPS
    echo "git remote set-url --push origin https://gitlab-ci-token:${AUTOVERSION_CI_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git"
    git remote set-url --push origin https://gitlab-ci-token:${AUTOVERSION_CI_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git
    git tag $releasedVersion
    git push --atomic origin HEAD:$CI_DEFAULT_BRANCH $releasedVersion
    echo "Se generó la versión $releasedVersion"
fi
