#!/bin/sh
echo "Agrega como directorio seguro ${CI_PROJECT_DIR}"
git config --global --add safe.directory ${CI_PROJECT_DIR}
echo "Agrega datos de usuario que disparó el job: $GITLAB_USER_NAME - $GITLAB_USER_EMAIL"
git config --global user.email "$GITLAB_USER_EMAIL"
git config --global user.name "$GITLAB_USER_NAME"
echo "Obteniendo titulo del ultimo commit"
SUBJECT=`git log -1 --pretty=%s`
if [ `echo $SUBJECT | grep -c "Merge "` -ge 1 ]
    then

        echo "Obteniendo version actual del ultimo tag creado. En su defecto sera la version 1.0.0"
        currentVersion=$(git describe --tag --abbrev=0 || echo "1.0.0")
        echo $currentVersion
        echo "Versión actual -> $currentVersion"

        if
            [ `echo $SUBJECT | grep -c "feature-"` -ge 1 ] ||
            [ `echo $SUBJECT | grep -c "minor-"` -ge 1 ]
            then
                echo 'minor change'
                newVersion=$(npm -loglevel=error -no-git-tag-version version minor | cut -c2-50)
        else
            if
                [ `echo $SUBJECT | grep -c "patch-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "fix-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "hotfix-"` -ge 1 ] ||
                [ `echo $SUBJECT | grep -c "doc-"` -ge 1 ]
            then
                echo 'patch change'
                newVersion=$(npm -loglevel=error -no-git-tag-version version patch | cut -c2-50)
            else
                if
                    [ `echo $SUBJECT | grep -c "major-"` -ge 1 ]
                    then
                        echo 'major change'
                        newVersion=$(npm -loglevel=error -no-git-tag-version version major | cut -c2-50)
                    else
                        echo "No se ha podido determinar la siguiente version. Por favor incluye alguna de las siguientes palabras en el nombre del branch:"
                        echo "patch-, fix-, hotfix- o doc- para incrementar el ultimo digito."
                        echo "feature- o minor- para incrementar el segundo digito."
                        echo "major- para incrementar el primer digito."
                        exit 1
                fi
            fi
        fi

        git add package.json
        git add package-lock.json

        git commit -m "Tag version $newVersion"
        # Configura URL para usar token y así no enviar usuario y contrasenia por HTTPS
        git remote set-url --push origin https://gitlab-ci-token:${MACHTIA_CI_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git

        git tag $newVersion
        git push --atomic origin HEAD:$CI_DEFAULT_BRANCH $newVersion
        echo "Se generó la versión $newVersion"
fi
